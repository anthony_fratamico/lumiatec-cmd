# Lumiatec Commander

**Lumiatec Commander** is a usefull tool to control LED lightings using command-line instructions.

## Prerequisites

Following Python packages are required:

* os
* sys
* platform
* argparse
* logging
* json
* pprint

Depending of device, following packages are required (see Installation section):

| Brand | Model | Library | Model input (--model/-m) |
|-|-|-|-|
| Lumiatec | PHS::16 | phs16 | lumiatec-phs16 (or phs16) |

## Installation

For Linux systems run :

* clone the code in the destination folder
```
git clone --recurse-submodules https://anthony_fratamico@bitbucket.org/anthony_fratamico/lumiatec-cmd.git
```

* go to the lumiatec-cmd directory
```
cd lumiatec-cmd
```

* and run
```
bash install.sh
```

## Example

* Download all information in a JSON file "out.json"
```
lumiatec-cmd --model lumiatec-phs16 --port /dev/ttyUSB0 --address 140 145 150 --get-infos --json --output-file out.json
```

* Print temperature data for spot 145
```
lumiatec-cmd --model lumiatec-phs16 --port /dev/ttyUSB0 --address 145 --get-infos temp
```

* Define master spot automatically
```
lumiatec-cmd --model lumiatec-phs16 --port /dev/ttyUSB0 --address 140 145 150 --set-master
```

* Turn on LED Hyper-red (660 nm) and Far-red (730 nm) synchronously at intensity 25%, Blue (460 nm) at 20%, in phase opposition, and green light at 3% continuoulsly, with PWM at 200 Hz and a dutty cycle of 50%
```
lumiatec-cmd --model lumiatec-phs16 --port /dev/ttyUSB0 --address 140 --set-freq 200 --set-colors HR_660 FR_730 25 0 0.5 BL_460 20 0.5 1 GR_525 3
```

## Built With

* [Python 3.5.2](https://www.python.org/) - Write and test


## Versioning

* **0.0.0**: developpement tests

## Authors

* **Anthony Fratamico**

## License

Writing in progress...
