#!/bin/bash

# Installation of Lumiatec Commander Suite

# Written by Anthony Fratamico, PhD at University of Liège (Belgium)
# Laboratory of Plant Physiology
# Writing date: 01/09/2020

SCRIPT_NAME="lumiatec-cmd.py"
PACKAGE_DIR="lumiatec"
SUBMODULE_DIR="submodules"
INIT_FILE="__init__.py"

# Make lumiatec-cmd file executable
echo "Making script executable"
chmod +x "$SCRIPT_NAME"

# Create lumiatec package folder
echo "Create package folder"
mkdir "$PACKAGE_DIR"

# Create symlinks to modules
echo "Create symlink to modules"
for dir in $SUBMODULE_DIR/* ; do
	for subdir in $dir/*/ ; do
		if [ -f "${subdir}${INIT_FILE}" ]; then
			echo "... module $(basename $subdir)"
			ln -s "$(realpath "$subdir")" "${PACKAGE_DIR}/$(basename "$subdir")"
			#echo "$(realpath "$subdir")"
			#echo "${PACKAGE_DIR}/$(basename "$subdir")"
		fi
	done
done

# Init file for lumiatec package
>"${PACKAGE_DIR}/${INIT_FILE}"

# End
echo "Completed !"
exit